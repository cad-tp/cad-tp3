#include "server.h"

#ifdef __SERVER_LOAD_BALANCER__

extern struct stack* stack;
extern boolean stop;

mpi_comm load_balance_comm_req;
mpi_comm load_balance_comm;
mpi_comm check_point_comm;


extern number rank;
extern number comm_size;

pthread_mutex_t l = PTHREAD_MUTEX_INITIALIZER;

/* creates the load balancing comm */
void
init_lb_comm()
{
  pthread_mutex_lock(&l);
  mpi_comm_dup(MPI_COMM_WORLD, &load_balance_comm);
  pthread_mutex_unlock(&l);
}

/* destroys the load balancing comm */
void
destroy_lb_comm()
{
  pthread_mutex_lock(&l);
  mpi_comm_free(&load_balance_comm);
  pthread_mutex_unlock(&l);
}

/* creates the checkpoint comm */
void
init_cp_comm()
{
  pthread_mutex_lock(&l);
  mpi_comm_dup(MPI_COMM_WORLD, &check_point_comm);
  pthread_mutex_unlock(&l);
}

/* destroys the checkpoint comm */
void
destroy_cp_comm()
{
  pthread_mutex_lock(&l);
  mpi_comm_free(&check_point_comm);
  pthread_mutex_unlock(&l);
}

/* the load balancer daemon function */
void*
deamon(void* data)
{
	byte tmp;
  mpi_status stats;
  unumber src;
  unumber tag;
  number *work;
  number no_work = -1;
  mpi_request req;
  number flag;

	while(stack->head != NULL)
	{
    __yal_log("waiting for work requests\n");

    pthread_mutex_lock(&l);
		mpi_irecv(&tmp, 0, mpi_byte_t, 
					   MPI_ANY_SOURCE, REQUEST_WORK_TAG, 
					   load_balance_comm, &req);
    pthread_mutex_unlock(&l);
    do
    {
      pthread_mutex_lock(&l);
      mpi_test(&req, &flag, &stats);
      pthread_mutex_unlock(&l);
      if(flag != true)
      {
        sched_yield();
        usleep(DEFAULT_WAIT_TIME);
      }
    }
    while(flag != true && stack->head != NULL);

    src = stats.MPI_SOURCE;
    tag = stats.MPI_TAG;

    assert(tag == REQUEST_WORK_TAG || stack->head == NULL);

    __yal_log("received work request from "NUM" with tag "NUM"\n", src, tag);
    if(pop(stack, (void **)&work))
    {
      __yal_log("I have work to send\n");
      pthread_mutex_lock(&l);
      mpi_isend(work, 1, mpi_number_t, src, 
                WORK_TAG, load_balance_comm, &req);
      pthread_mutex_unlock(&l);

      do
      {
        pthread_mutex_lock(&l);
        mpi_test(&req, &flag, &stats);
        pthread_mutex_unlock(&l);
        if(flag != true)
        {
          sched_yield();
          usleep(DEFAULT_WAIT_TIME);
        }
      }
      while(flag != true && stack->head != NULL);

      free(work);
    }
    else
    {
      pthread_mutex_lock(&l);
      mpi_isend(&no_work, 1, mpi_number_t, src, 
                WORK_TAG, load_balance_comm, &req);
      pthread_mutex_unlock(&l);

      do
      {
        pthread_mutex_lock(&l);
        mpi_test(&req, &flag, &stats);
        pthread_mutex_unlock(&l);
        if(flag != true)
        {
          sched_yield();
          usleep(DEFAULT_WAIT_TIME);
        }
      }
      while(flag != true);
    }
	}
  
  __yal_log("I don't have work to send ANYMORE\n");

  pthread_t pth;
  pthread_create(&pth, NULL, checkpoint, NULL);

	while(!stop)
	{
    pthread_mutex_lock(&l);
		mpi_irecv(&tmp, 0, mpi_byte_t,
             MPI_ANY_SOURCE, REQUEST_WORK_TAG,
             load_balance_comm, &req);
    pthread_mutex_unlock(&l);

    do
    {
      pthread_mutex_lock(&l);
      mpi_test(&req, &flag, &stats);
      pthread_mutex_unlock(&l);
      if(flag != true)
      {
        sched_yield();
        usleep(DEFAULT_WAIT_TIME);
      }
    }
    while(flag != true && !stop);

    src = stats.MPI_SOURCE;
    tag = stats.MPI_TAG;

    pthread_mutex_lock(&l);
		mpi_isend(&no_work, 1, mpi_number_t, src,
              WORK_TAG, load_balance_comm, &req);
    pthread_mutex_unlock(&l);

    do
    {
      pthread_mutex_lock(&l);
      mpi_test(&req, &flag, &stats);
      pthread_mutex_unlock(&l);
      if(flag != true)
      {
        sched_yield();
        usleep(DEFAULT_WAIT_TIME);
      }
    }
    while(flag != true  && !stop);
	}

	pthread_join(pth, NULL);
	pthread_exit(NULL);
}

/* the checkpoint function */
void*
checkpoint(void* data)
{
  mpi_request req;
  mpi_status st;
  number flag;
  __yal_log("OK, my stack is empty\nI am waiting"
            " for my colleagues to checkpoint\n");

  pthread_mutex_lock(&l);
	mpi_ibarrier(check_point_comm, &req);
  pthread_mutex_unlock(&l);

  do
  {
    pthread_mutex_lock(&l);
    mpi_test(&req, &flag, &st);
    pthread_mutex_unlock(&l);
    if(flag != true)
    {
      sched_yield();
      usleep(DEFAULT_WAIT_TIME);
    }
  }
  while(flag != true);


	__sync_synchronize();
	stop = true;
  __yal_log("everybody can stop now\n");
	
	pthread_exit(NULL);
}

#endif  /* __SERVER_LOAD_BALANCER__ */