#include "statistics.h"
#ifdef STATISTICS_H_

//the statistical tests to be made.
boolean rt_avg = 1;
const char* rt_avg_name = "Realtime average";
boolean ut_avg = 1;
const char* ut_avg_name = "Usertime average";
boolean rt_v = 1;
const char* rt_v_name = "Realtime variance";
boolean ut_v = 1;
const char* ut_v_name = "Usertime variance";
boolean num_cpus = 1;
const char* num_cpus_name = "#Cpus";
boolean cpu_occupation = 1;
const char* cpu_occupation_name = "Cpu occupation";
boolean speed_up = 1;
const char* speed_up_name = "Speedup";

statistics_t *
create_statistics(number nworkers, number ntests)
{
  statistics_t * new_t = (statistics_t *) malloc(sizeof(statistics_t));
  new_t->real_times = (real *) malloc(ntests * sizeof(real));
  new_t->user_times = (real *) malloc(ntests * sizeof(real));
  new_t->test_counter = 0;
  new_t->num_tests = ntests;
  new_t->num_workers = nworkers;
  new_t->filter = 0.0;
  return new_t;
}

void
delete_statistics(statistics_t * s)
{
  free(s->real_times);
  free(s->user_times);
  free(s);
}

void
set_statistics(boolean rt_avg_, boolean ut_avg_, boolean rt_v_, 
  boolean ut_v_, boolean num_cpus_, boolean cpu_occupation_, boolean speed_up_)
{
  rt_avg            = rt_avg_;
  ut_avg            = ut_avg_;
  rt_v              = rt_v_;
  ut_v              = ut_v_;
  num_cpus          = num_cpus_;
  cpu_occupation    = cpu_occupation_;
  speed_up          = speed_up_;
}

real
get_speedup(statistics_t* s_n, statistics_t * s_1)
{
  return get_real_time_average(s_1) / get_real_time_average(s_n);
}

void
show_statistics(statistics_t * s, statistics_t * s_1)
{
  if(!(s->test_counter == s->num_tests && s->num_tests))
  {
    return;
  }
  if(num_cpus)
  {
    printf("%s\t" NUM "\n", num_cpus_name, s->num_workers);
  }
  if(rt_avg)
  {
    printf("%s\t"REAL"\n", rt_avg_name, get_real_time_average(s));
  }
  if(rt_v)
  {
    printf("%s\t"REAL"\n", rt_v_name, get_real_time_variance(s));
  }
  if(ut_avg)
  {
    printf("%s\t"REAL"\n", ut_avg_name, get_user_time_average(s));
  }
  if(ut_v)
  {
    printf("%s\t"REAL"\n", ut_v_name, get_user_time_variance(s));
  }
  if(cpu_occupation)
  {
    printf("%s\t\t"REAL"\n", cpu_occupation_name, get_cpu_occupation(s));
  }
  if(speed_up)
  {
    printf("%s\t\t\t"REAL"\n", speed_up_name, get_speedup(s, s_1));
  }
  printf("\n\n\n");
}

void
save_statistics(statistics_t * s, statistics_t * s_1, char* filename)
{
  if(!(s->test_counter == s->num_tests && s->num_tests))
  {
    return;
  }

  FILE * f = fopen(filename, "rb");
  number exists = f != NULL;
  
  if(exists)
  {
    fclose(f);
  }

  f = fopen(filename, "a+");

  char* separator = " , ";

  if(!exists)
  {
    fprintf(f, "%s%s", num_cpus_name, separator);
    fprintf(f, "%s%s", rt_avg_name, separator);
    fprintf(f, "%s%s", rt_v_name, separator);
    fprintf(f, "%s%s", ut_avg_name, separator);
    fprintf(f, "%s%s", ut_v_name, separator);
    fprintf(f, "%s%s", cpu_occupation_name, separator);
    fprintf(f, "%s\n", speed_up_name);
  }

  if(num_cpus)
  {
    fprintf(f, NUM"%s", s->num_workers, separator);
  }
  if(rt_avg)
  {
    fprintf(f, REAL"%s", get_real_time_average(s), separator);
  }
  if(rt_v)
  {
    fprintf(f, REAL"%s", get_real_time_variance(s), separator);
  }
  if(ut_avg)
  {
    fprintf(f, REAL"%s", get_user_time_average(s), separator);
  }
  if(ut_v)
  {
    fprintf(f, REAL"%s", get_user_time_variance(s), separator);
  }
  if(cpu_occupation)
  {
    fprintf(f, REAL"%s", get_cpu_occupation(s), separator);
  }
  if(speed_up)
  {
    fprintf(f, REAL, get_speedup(s, s_1));
  }

  fprintf(f, "\n");
  fclose(f);
}

void
set_filter(statistics_t *s, real filter)
{
  s->filter = filter;
}

number
compare(const void * a, const void * b)
{
  real x1 = *(real *) a;
  real x2 = *(real *) b;

  if(x1 == x2)
  {
    return 0;
  }
  if(x1 < x2)
  {
    return -1;
  }
  else
  {
    return 1;
  }
}

real
get_average(real * results, number size, real filter)
{
  real average = 0.0;
  qsort(results, size, sizeof(real), compare);
  number base = size * filter;
  number limit = size - size * filter;
  number i;
  for(i = base; i < limit; i++)
  {
    average = average + results[i] / ((real) limit - base);
  }
  return average;
}

real
get_variance(real * results, number size, real average, real filter)
{
  qsort(results, size, sizeof(real), compare);
  number base = size * filter;
  number limit = size - size * filter;
  real variance = 0.0;
  number i;
  for(i = base; i < limit; i++)
  {
    variance = variance + pow(results[i] - average, 2.0);
  }

  variance = variance / ((real) (size - filter * size * 2.0) - 1.0);
  return variance;
}

real
get_real_time_average(statistics_t *s)
{
  if(!(s->test_counter == s->num_tests && s->num_tests))
  {
    return -1.0;
  }
  return get_average(s->real_times, s->num_tests, s->filter);
}

real
get_real_time_variance(statistics_t *s)
{
  if(!(s->test_counter == s->num_tests && s->num_tests))
  {
    return -1.0;
  }
  return get_variance(s->real_times, s->num_tests, get_real_time_average(s),
      s->filter);
}

real
get_user_time_average(statistics_t *s)
{
  if(!(s->test_counter == s->num_tests && s->num_tests))
  {
    return -1.0;
  }
  return get_average(s->user_times, s->num_tests, s->filter);
}

real
get_user_time_variance(statistics_t *s)
{
  if(!(s->test_counter == s->num_tests && s->num_tests))
  {
    return -1.0;
  }
  return get_variance(s->user_times, s->num_tests, get_user_time_average(s),
      s->filter);
}

void
add_time(statistics_t *s, real real_time, real user_time)
{
  if(!(s->test_counter < s->num_tests && s->num_tests))
  {
    return;
  }
  s->real_times[s->test_counter] = real_time;
  s->user_times[s->test_counter] = user_time;
  s->test_counter++;
}

real
get_cpu_occupation(statistics_t *s)
{
  if(!(s->test_counter == s->num_tests && s->num_tests))
  {
    return -1.0;
  }
  return get_user_time_average(s) / get_real_time_average(s);
}

#endif
