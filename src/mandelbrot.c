#include "mandelbrot.h"

#ifdef __MANDELBROT__
#define __MANDELBROT__

/* defines the number of threads to execute in parallel */
unumber num_threads = 1;

/* computes a mandelbrot histogram for a set of points */
void
(*compute_function) (byte*, unumber*, struct mb_grid*) = compute_omp_dynamic;

/* mpi data type of a mb_grid */
mpi_datatype mb_grid_datatype;

/* sets the number of threads executing */
void
set_num_threads(unumber nthr)
{
  num_threads = nthr;
}

/* sets the type of computation execution */
void
set_computation_type(computation_type_t type)
{
  switch(type)
  {
    case static_comp:
    {
      compute_function = compute_omp_static;
      break;
    }
    case dynamic_comp:
    {
      compute_function = compute_omp_dynamic;
      break;
    }
    default:
    {
      compute_function = compute_omp_dynamic;
      break;
    }
  }
}

/* creates an mpi data type for the grid */
void
create_mpi_mb_grid_type()
{
  struct mb_grid dummy;
  /* Number of elements in each position of the derivated datatype */ 
  number len[NUM_FIELDS_MB_GRID] =
  {
    1,
    1,
    1,
    1,
    1,
    1
  }; 

  /* Getting the memory addresses to displacement */
  mpi_aint disp[NUM_FIELDS_MB_GRID]; 

  mpi_datatype type[NUM_FIELDS_MB_GRID] = 
  {
    mpi_real_t,
    mpi_real_t,
    mpi_real_t,
    mpi_real_t,
    mpi_unumber_t,
    mpi_unumber_t
  };

  mpi_get_address(&dummy.xi, &disp[0]); 
  mpi_get_address(&dummy.xf, &disp[1]); 
  mpi_get_address(&dummy.yi, &disp[2]); 
  mpi_get_address(&dummy.yf, &disp[3]);
  mpi_get_address(&dummy.width, &disp[4]);
  mpi_get_address(&dummy.len, &disp[5]);

  unumber i;
  
  for (i=1; i < NUM_FIELDS_MB_GRID; i++){
    disp[i] = disp[i] - disp[0];

  }
  disp[0] = 0;

  mpi_type_create_struct(NUM_FIELDS_MB_GRID, len, disp, type, &mb_grid_datatype);
  mpi_type_commit(&mb_grid_datatype);
}

/* destroys the mpi data type for the grid */
void
destroy_mpi_mb_grid_type()
{
  mpi_type_free(&mb_grid_datatype);
}

/* creates an output struct */
struct part_output_grid*
create_output_grid(byte* b, number offset)
{
  struct part_output_grid* pog = 
        (struct part_output_grid*) malloc(sizeof(struct part_output_grid));
  pog->offset = offset;
  pog->b = b;
  /* returning some Programação orientada à gambiarra */
  return pog;
}

/* destroys an output struct */
void
destroy_output_grid(struct part_output_grid* pog)
{
  free(pog->b);
  free(pog);
}

/* creates a mb_grid structure */
struct mb_grid* 
create_mb_grid(
	real xi,
	real xf,
	real yi,
	real yf,
	unumber w,
	unumber l)
{
	struct mb_grid* new_grid = (struct mb_grid*) malloc(sizeof(struct mb_grid));
	new_grid->xi = xi;
	new_grid->xf = xf;
	new_grid->yi = yi;
	new_grid->yf = yf;
	new_grid->width = w;
	new_grid->len = l;
	return new_grid;
}

/* computes a mandelbrot histogram for a set of points */
unumber
compute_point(real ci, real cr)
{
  unumber iterations = 0;
  real zi = 0;
  real zr = 0;
  
  while(((zr * zr) + (zi * zi) < 4) && (iterations < MAX_ITERATIONS))
  {
    real nr = zr*zr - zi*zi + cr;
    real ni = 2*zr*zi + ci;
    zi = ni;
    zr = nr;
    iterations ++;
  }

  return iterations;  
}

/* computes a mandelbrot histogram for a set of points */
void
compute_mandelbrot (byte* b, unumber* hist, struct mb_grid* grid)
{
  compute_function(b, hist, grid);
}

/* calculates the number of iterations for a point */
void
compute_omp_dynamic(byte *buffer, unumber* hist, struct mb_grid* grid)
{
  number nx = grid->width;
  number ny = grid->len;
  real xi   = grid->xi;
  real xf   = grid->xf;
  real yi   = grid->yi;
  real yf   = grid->yf;

  real delta_x, delta_y;

  delta_x = (xf - xi)/nx;
  delta_y = (yf - yi)/ny;
  number i;
  #pragma omp parallel for num_threads(num_threads)   \
              default(none) \
              shared(hist, buffer, nx, ny, yi, yf, xi, xf, delta_x, delta_y) \
              schedule(dynamic, 4)
  for(i = 0; i < ny*nx; i++)
  {
    number x, y;
    x = i % nx;
    y = i / nx;
    real y_value = yi + delta_y * y;
    real x_value = xi + delta_x * x;
    buffer[y*nx + x] = compute_point(x_value, y_value);
    if(hist != NULL)
    {
      __sync_fetch_and_add(&hist[buffer[y*nx + x]], 1);
    }
  }
}

/* shows the values of a mb_grid */
void
show_mb_grid(struct mb_grid* mb)
{
  __yal_log("mb_grid:\n");
  __yal_log("\txi:\t"REAL"\n", mb->xi);
  __yal_log("\txf:\t"REAL"\n", mb->xf);
  __yal_log("\tyi:\t"REAL"\n", mb->yi);
  __yal_log("\tyf:\t"REAL"\n", mb->yf);
  __yal_log("\twidth:\t"UNUM"\n", mb->width);
  __yal_log("\tlength:\t"UNUM"\n", mb->len);
}

/* calculates the number of iterations for a point */
void
compute_omp_static(byte *buffer, unumber* hist, struct mb_grid* grid)
{
  number nx = grid->width;
  number ny = grid->len;
  real xi   = grid->xi;
  real xf   = grid->xf;
  real yi   = grid->yi;
  real yf   = grid->yf;

  real delta_x, delta_y;

  delta_x = (xf - xi)/nx;
  delta_y = (yf - yi)/ny;
  number i;
  #pragma omp parallel for num_threads(num_threads)   \
              default(none) \
              shared(hist, buffer, nx, ny, yi, yf, xi, xf, delta_x, delta_y) \
              schedule(static, 128)
  for(i = 0; i < ny*nx; i++)
  {
    number x = i % nx;
    number y = i / nx;
    real y_value = yi + delta_y * y;
    real x_value = xi + delta_x * x;
    buffer[y*nx + x] = compute_point(x_value, y_value);
    if(hist != NULL)
    {
      __sync_fetch_and_add(&hist[buffer[y*nx + x]], 1);
    }
  }
}

/* deallocates a mb_grid struct */
void
destroy_mb_grid(struct mb_grid* grid)
{
	free(grid);
}

#endif /* __MANDELBROT__ */
