#include "computations.h"

struct stack* stack;
struct stack* computed;

#ifdef __COMPUTATIONS__

extern struct execution_info ei;
extern pthread_mutex_t l;
extern number rank;
extern number comm_size;
extern mpi_datatype mb_output_datatype;

volatile boolean stop = false;

extern mpi_comm load_balance_comm_req;
extern mpi_comm load_balance_comm;
extern mpi_comm check_point_comm;

void
(*compute)(byte*, unumber* hist, struct execution_info_slave*);

void
init_structures()
{
  stack = create_stack();
  computed = create_stack();
}

void
destroy_structures()
{
  destroy_stack(stack);
  destroy_stack(computed);
}

void
execute_eval(byte* b, unumber* hist, struct execution_info_slave* eis)
{
  number curr_cpus = 1;
  number n_tests = eis->runs;
  real filter = 0.0;

  if(rank == 0)
  {
    filter = ei.s_i.discard;
  }
  statistics_t* st_1 = NULL;
  statistics_t* st = NULL;

  while(curr_cpus <= eis->n_threads)
  {
    if(rank == 0)
    {
      /* master */
      st = create_statistics(curr_cpus, n_tests);
      set_filter(st, filter);
      if(curr_cpus == 1)
      {
        st_1 = st; 
      }
    }
    
    set_num_threads(curr_cpus);
    number i;
    for(i = 0; i < eis->runs; i++)
    {
      if(eis->histogram && hist)
      {
        number j;
        for(j = 0; j < NUM_COLOR_VALUES; j++)
        {
          hist[j] = 0;
        }
      }
      
      mpi_barrier(MPI_COMM_WORLD);
      real beg_run = mpi_wtime();

      compute(b, hist, eis);

      mpi_barrier(MPI_COMM_WORLD);
      real end_run = mpi_wtime();
      real time_run = end_run - beg_run;
      stop = false;
    
      if(rank == 0)
      {
        add_time(st, time_run, time_run);
      }
    }

    if(rank == 0)
    {
      if(ei.eval)
      {
        if(ei.show_statistics)
        {
          show_statistics(st, st_1);
        }
        if(ei.save_statistics)
        {
          save_statistics(st, st_1, ei.statistics_fn);
        }
      }
    }
    curr_cpus = curr_cpus << 1;
  }

}

void
execute_run(byte* b, unumber* hist, struct execution_info_slave* eis)
{
  compute(b, hist, eis);
}

void
compute_dynamic(byte* b, unumber* hist, struct execution_info_slave* eis)
{
  mpi_request req;
  mpi_status st;
  number flag;

  if(eis->execution_mode == DYNAMIC_MODE)
  {
    if(!rank)
    {
      __yal_log("Master is adding computations to the queue\n");
      unumber i;
      for(i = 0; i < eis->data.len; i++)
      {
        number* task = (number*) malloc(sizeof(number));
        *task = (number) i * eis->data.width;
        push(stack, task);
      }
    }
  }

  if(eis->execution_mode == WS_MODE)
  {
    unumber workload = eis->data.len / comm_size;
    unumber i;
    for(i = 0; i < workload; i++)
    {
      unumber* task = (unumber*) malloc(sizeof(unumber));
      *task = (rank * workload * eis->data.width) + i * eis->data.width;
      push(stack, task);
    }
  }

  pthread_t pth;
  pthread_create(&pth, NULL, deamon, NULL);

  pthread_mutex_lock(&l);
  mpi_ibarrier(MPI_COMM_WORLD, &req);
  pthread_mutex_unlock(&l);

  do
  {
    pthread_mutex_lock(&l);
    mpi_test(&req, &flag, &st);
    pthread_mutex_unlock(&l);
    if(flag != true)
    {
      sched_yield();
      usleep(DEFAULT_WAIT_TIME);
    }
  }
  while(flag != true);

  number offset = -1;

  unumber workload = eis->data.width;

  real delta_y = (eis->data.yf - eis->data.yi) / (real) eis->data.len;

  struct mb_grid *gr = create_mb_grid(eis->data.xi, eis->data.xf, 0.0,
                                        0.0, eis->data.width, 1);

  byte* line = NULL;
  while(!stop)
  {
    if(!get_work(&offset, eis->execution_mode))
    {
      sched_yield();
      usleep(DEFAULT_WAIT_TIME);
      continue;
    }

    if(stop)
    {
      break;
    }
    gr->yi = eis->data.yi + (offset / workload) * delta_y;
    gr->yf = gr->yi + delta_y;
    __yal_log("computing mandelbrot\n");
    show_mb_grid(gr);
    line = (byte*) malloc(eis->data.width * sizeof(byte));
    compute_mandelbrot(line, hist, gr);
    struct part_output_grid* pog = create_output_grid(line, offset);
    push(computed, pog);
  }
  __yal_log("looks like everyone has finished the computation\n");

  /* wait for everyone to finish execution */
  pthread_mutex_lock(&l);
  mpi_ibarrier(MPI_COMM_WORLD, &req);
  pthread_mutex_unlock(&l);

  do
  {
    pthread_mutex_lock(&l);
    mpi_test(&req, &flag, &st);
    pthread_mutex_unlock(&l);
    if(flag != true)
    {
      sched_yield();
      usleep(DEFAULT_WAIT_TIME);
    }
  }
  while(flag != true);

  pthread_join(pth, NULL);

  return;

  struct part_output_grid* pog;
  
  unumber lines = 0;

  if(eis->print_image)
  {
    /* join the computations */
    if(eis->join_output)
    {
      if(!rank)
      {
        while(computed->head != NULL)
        {
          pop(computed, (void **) &pog);
          offset = pog->offset;
          line = pog->b;
          __sync_fetch_and_add(&lines,1);
          memcpy(&b[offset], line, eis->data.width * sizeof(byte));
          destroy_output_grid(pog);
        }

        line = (byte *) malloc(sizeof(byte) * eis->data.width);

        while(lines < eis->data.len)
        {
          mpi_status status;

          mpi_recv(&offset, 1, mpi_number_t,
               MPI_ANY_SOURCE, JOIN_TAG,
               MPI_COMM_WORLD, &status);

          mpi_recv(line, eis->data.width, mpi_byte_t,
               status.MPI_SOURCE, JOIN_TAG,
               MPI_COMM_WORLD, &status);

          __sync_fetch_and_add(&lines,1);
          memcpy(&b[offset], line, eis->data.width * sizeof(byte));
        }

        free(line);
        /* fill own buffer */
        /* then receive from others */
      }
      else
      {
        /* send to master */
        while(computed->head != NULL)
        {
          pop(computed, (void **) &pog);

          mpi_send(&pog->offset, 1, mpi_number_t,
                   0, JOIN_TAG, MPI_COMM_WORLD);
          mpi_send(pog->b, eis->data.width, mpi_byte_t,
                   0, JOIN_TAG, MPI_COMM_WORLD);
          destroy_output_grid(pog);
        }
      }

      /* end of single master image output */
    }
    else
    {

      /* to solve */
      /* send to others and receive from others */
      unumber wl = (eis->data.len / comm_size);
      unumber beg = wl * rank;

      lines = 0;
      
      struct stack* reqs = create_stack();
      mpi_request *t = NULL;


      
      while(computed->head != NULL)
      {
        pop(computed, (void **) &pog);

        unumber line_num = pog->offset / eis->data.width;
        
        if(line_num >= beg && line_num < beg + wl)
        {
          /* for me */
          memcpy(&b[pog->offset - beg * eis->data.width], 
            pog->b, eis->data.width * sizeof(byte));
          lines++;

          /* ml */
        }
        else
        {
          t = (mpi_request*) malloc(2*sizeof(mpi_request));
          mpi_isend(&pog->offset, 1, mpi_number_t,
                   0, JOIN_TAG, MPI_COMM_WORLD, t);
          mpi_isend(pog->b, eis->data.width, mpi_byte_t,
                   0, JOIN_TAG, MPI_COMM_WORLD, &t[1]);
          push(reqs, &t);
        }
      }

      line = (byte *) malloc(sizeof(byte) * eis->data.width);
      mpi_status status;

      while(lines < wl)
      {

        mpi_recv(&offset, 1, mpi_number_t,
             MPI_ANY_SOURCE, JOIN_TAG,
             MPI_COMM_WORLD, &status);

        mpi_recv(line, eis->data.width, mpi_byte_t,
             status.MPI_SOURCE, JOIN_TAG,
             MPI_COMM_WORLD, &status);

        __sync_fetch_and_add(&lines,1);
        memcpy(&b[pog->offset - beg * eis->data.width], 
          line, eis->data.width * sizeof(byte));
      }

      free(line);
      
      t = NULL;

      number flag;

      while(reqs->head != NULL)
      {
        pop(reqs, (void **) &t);
        do
        {
          mpi_test(t, &flag, &status);
          if(flag != true)
          {
            usleep(DEFAULT_WAIT_TIME);
          }
        }
        while(flag != true);

        do
        {
          mpi_test(&t[1], &flag, &status);
          if(flag != true)
          {
            usleep(DEFAULT_WAIT_TIME);
          }
        }
        while(flag != true);
      }

      //destroy_stack(reqs);
      /* fill in my buffer */

      /* send the rest to others */

    }
  }

  if(eis->histogram)
  {
    /* sums up all the histograms */
    if(!rank)
    {
      mpi_reduce(MPI_IN_PLACE, hist, 256, 
        mpi_unumber_t, MPI_SUM, 0, MPI_COMM_WORLD);
    }
    else
    {
      mpi_reduce(hist, hist, 256, 
        mpi_unumber_t, MPI_SUM, 0, MPI_COMM_WORLD);
    }
  }
}

void
compute_static(byte* b, unumber* hist, struct execution_info_slave* eis)
{
  if(rank)
  {
    show_exec_info_slave(eis);
  }

  real portion = (eis->data.yf - eis->data.yi) / comm_size;
  unumber workload = eis->data.len / comm_size;
  real yi = eis->data.yi + portion * rank;
  real yf = yi + portion;

  struct mb_grid *gr = create_mb_grid(eis->data.xi, eis->data.xf, yi,
                                        yf, eis->data.width, workload);

  compute_mandelbrot(b, hist, gr);
  unumber buff_sz = sizeof(byte) * eis->data.len/comm_size * eis->data.width;

  if(eis->join_output)
  {
    mpi_gather(b, buff_sz, mpi_byte_t, b, buff_sz, mpi_byte_t, 0, MPI_COMM_WORLD);
  }

  if(eis->histogram)
  {
    /* sums up all the histograms */
    if(!rank)
    {
      mpi_reduce(MPI_IN_PLACE, hist, 256, 
        mpi_unumber_t, MPI_SUM, 0, MPI_COMM_WORLD);
    }
    else
    {
      mpi_reduce(hist, hist, 256, 
        mpi_unumber_t, MPI_SUM, 0, MPI_COMM_WORLD);
    }
  }
}

/* tries to fecth work locally or remotely */
boolean
get_work(number* work, unumber execution_mode)
{
  byte tmp = 'x';
  unumber tag;
  unumber victim = 0;
  mpi_status stats;
  mpi_request req;
  number flag;

  __yal_log("getting work\n");
  if(stack->head != NULL)
  {
    __yal_log("i may have work\n");
    unumber* lw;
    if(pop(stack, (void **)&lw))
      {
        __yal_log("i really have work\n");
        *work = *lw;
        free(lw);
        return true;
      }
  }

  *work = -1;
  while(!stop)
  {
    
    if(execution_mode == DYNAMIC_MODE)
    {
      __yal_log("static server mode\n");
      victim = 0;
    }
    else if (execution_mode == WS_MODE)
    {
      __yal_log("dynamic victim mode\n");
      if(comm_size == 1)
      {
        *work = -1;
        return false;
      }
      do
      {
        victim = random() % comm_size;
      }
      while(victim == rank && !stop);
    }
    else
    {
      /* default mode */
      if(comm_size == 1)
      {
        *work = -1;
        return false;
      }
      do
      {
        victim = random() % comm_size;
      }
      while(victim == rank && !stop);
    }

    assert(victim >= 0 && victim != comm_size);
  
    __yal_log("sending the request to the victim "UNUM"\n", victim);

    pthread_mutex_lock(&l);
    mpi_isend(&tmp, 0, mpi_byte_t, 
              victim, REQUEST_WORK_TAG, load_balance_comm, &req);
    pthread_mutex_unlock(&l);

    do
    {
      pthread_mutex_lock(&l);
      mpi_test(&req, &flag, &stats);
      pthread_mutex_unlock(&l);
      if(flag != true)
      {
        sched_yield();
        usleep(DEFAULT_WAIT_TIME);
      }
    }
    while(flag != true && !stop);
    if(stop)
    {
      *work = -1;
      return false;
    }

    __yal_log("waiting for the request reply \n");

    pthread_mutex_lock(&l);
    mpi_irecv(work, 1, mpi_number_t, 
              victim, WORK_TAG, 
              load_balance_comm, &req);
    pthread_mutex_unlock(&l);

    do
    {
      pthread_mutex_lock(&l);
      mpi_test(&req, &flag, &stats);
      pthread_mutex_unlock(&l);
      if(flag != true)
      {
        sched_yield();
        usleep(DEFAULT_WAIT_TIME);
      }
    }
    while(flag != true && !stop);

    if(stop)
    {
      *work = -1;
      return false;
    }

    __yal_log("received work? offset is "NUM"\n", *work);
    tag = stats.MPI_TAG;
    assert(tag != INVALID_REQUEST_TAG);

    if(*work != -1)
    {
      return true;
    }
  }
  return false;
}

#endif /* __COMPUTATIONS__ */