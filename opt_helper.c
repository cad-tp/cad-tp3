#include "opt_helper.h"

#ifdef __OPT_HELPER__

const char* argp_program_bug_address = MAIL_ADDRESS;
const char* argp_program_version     = VERSION;

extern mpi_datatype mb_grid_datatype;

/* mpi data type of a execution info slave */
mpi_datatype eis_datatype;

number
parse_opt(number key, char* arg, struct argp_state* state)
{
  struct execution_info* ei = state->input;
  struct stats_info* si = &((struct execution_info*)(state->input))->s_i;
  struct mb_grid* gi = &((struct execution_info*)(state->input))->data;
  switch(key)
  {
    case 'b':
    case 'e':
    {
      if(arg != NULL)
      {
        ei->runs = atoi(arg);
      }
      ei->eval = true;
      break;
    }
    case 'p':
    {
      if(arg != NULL)
      {
        ei->image_fn = arg;
      }
      else
      {
        ei->image_fn = DEFAULT_OUTPUT;
      }
      ei->print_image = true;
      break;
    }
    case 'w':
    {
      unumber width = 1;
      if(arg != NULL)
      {
        width = atoi(arg);
      }
      else
      {
        printf("width missing\n");
        exit(-1);
      }
      gi->width = width;
      break;
    }
    case 'h':
    {
      unumber height = 1;
      if(arg != NULL)
      {
        height = atoi(arg);
      }
      else
      {
        printf("height missing\n");
        exit(-1);
      }
      gi->len = height;
      break;
    }
    case 't':
    {
      unumber threads = 1;
      if(arg != NULL)
      {
        threads = atoi(arg);
      }
      else
      {
        printf("number of threads missing\n");
        exit(-1);
      }
      ei->n_threads = threads;
      break;
    }
    case 's':
    {
      char* fn = DEFAULT_STAT_OUTPUT;
      if(arg != NULL)
      {
        fn = arg;
      }
      ei->save_statistics = true;
      ei->statistics_fn = fn;
      break;
      break;
    }
    case 297:
    {
      ei->omp_dynamic = false;
      break;
    }
    case 298:
    {
      ei->omp_dynamic = true;
      break;
    }
    case 'j':
    {
      ei->join_output = true;
      break;
    }
    case 299:
    {
      ei->join_output = false;
      break;
    }
    case 300:
    {
      si->ut_avg = false;
      break;
    }
    case 301:
    {
      si->rt_avg = false;
      break;
    }
    case 302:
    {
      si->ut_v = false;
      break;
    }
    case 303:
    {
      si->rt_v = false;
      break;
    }
    case 304:
    {
      si->num_cpus = false;
      break;
    }
    case 305:
    {
      si->cpu_occupation = false;
      break;
    }
    case 306:
    {
      si->speed_up = false;
      break;
    }
    case 307:
    {
      ei->show_statistics = false;
      break;
    }
    case 308:
    {
      ei->show_total_time = true;
      break;
    }
    case 309:
    {
      ei->use_omp = false;
      break;
    }
    case 310:
    {
      real to_discard = 0.0f;
      if(arg != NULL)
      {
        to_discard = atof(arg);
      }
      else
      {
        printf("discard ration missing\n");
        exit(-1);
      }
      si->discard = to_discard;
      break;
    }
    case 311:
    {
      real xi;
      if(arg != NULL)
      {
        xi = atof(arg);
      }
      else
      {
        printf("xi value missing\n");
        exit(-1);
      }
      gi->xi = xi;
      break;
    }
    case 312:
    {
      real xf;
      if(arg != NULL)
      {
        xf = atof(arg);
      }
      else
      {
        printf("xf value missing\n");
        exit(-1);
      }
      gi->xf = xf;
      break;
    }
    case 313:
    {
      real yi;
      if(arg != NULL)
      {
        yi = atof(arg);
      }
      else
      {
        printf("yi value missing\n");
        exit(-1);
      }
      gi->yi = yi;
      break;
    }
    case 314:
     {
      real yf;
      if(arg != NULL)
      {
        yf = atof(arg);
      }
      else
      {
        printf("yf value missing\n");
        exit(-1);
      }
      gi->yf = yf;
      break;
    }
    case 315:
    {
      ei->execution_mode = STATIC_MODE;
      break;
    }
    case 316:
    {
      ei->execution_mode = DYNAMIC_MODE;
      break;
    }
    case 318:
    {
      ei->execution_mode = WS_MODE;
      break;
    }
    case 319:
    {
      ei->histogram = false;
      break;
    }
    case 320:
    {
      char* fn;
      if(arg != NULL)
      {
        fn = arg;
      }
      else
      {
        printf("histogram filename missing\n");
        exit(-1);
      }
      ei->histogram_fn = fn;
      break;
    }
  }

  return 0;
}


/* master */
struct argp_option options[] =
{
  { 
    "print-image", 'p', "OUTPUT", OPTION_ARG_OPTIONAL, 
    "Prints the result to a pgm file"
  },
  { 
    "evaluate", 'e', "NUMBER OF TESTS", 0, 
    "Evaluates the execution, running the program the given amount of times"
  },
  {
   "benchmark", 'b', "NUMBER OF TESTS", OPTION_ALIAS, 
    0 
  },
  {
    "width", 'w', "WIDTH", 0, 
    "The width dimension for the mandelbrot image" 
  },
  {
    "height", 'h', "HEIGHT", 0, 
    "The height dimension for the mandelbrot image" 
  },
  {
    "threads", 't', "NUMBER OF THREADS", 0, 
    "Number of concurrent threads" 
  },
  {
    "static", 297, 0, 0, 
    "The computation within each node is performed statically"
  },
  {
    "dynamic", 298, 0, 0, 
    "The computation within each node is performed dynamically"
  },
  {
    "join-output", 'j', 0, 0, 
    "The master writes one single output" 
  },
  {
    "no-join-output", 299, 0, 0, 
    "Each computation node outputs a part of the image\n"
    "This way the performance is increased" 
  },
  { 
    "no-usertime-average", 300, 0, 0, 
    "Does not calculate the usertime average"
  },
  { 
    "no-realtime-average", 301, 0, 0, 
    "Does not calculate the realtime average"
  },
  { 
    "no-usertime-variance", 302, 0, 0, 
    "Does not calculate the usertime variance"
  },
  { 
    "no-realtime-variance", 303, 0, 0, 
    "Does not calculate the realtime variance"
  },
  { 
    "no-cpu-number", 304, 0, 0, 
    "Does not show show the number of threads running"
  },
  { 
    "no-cpu-occupation", 305, 0, 0, 
    "Does not show the cpu occupation"
  },
  { 
    "no-speedup-comparison", 306, 0, 0, 
    "Does not show the speed ups achieved"
  },
  { 
    "save-statistics", 's', "FILENAME", OPTION_ARG_OPTIONAL, 
    "Saves the statistics to a CSV file"
  },
  { 
    "no-show-statistics", 307, 0, 0, 
    "Does not print the statistics on the screen"
  },
  {
    "time", 308, 0, OPTION_HIDDEN, 
    "Shows the time taken to run the program" 
  },
  {
    "no-omp", 309, 0, 0, 
    "Sequencial execution" 
  },
  { 
    "discard", 310, "RATIO", 0, 
    "Does not print the statistics on the screen"
  },
  { 
    "xi", 311, "VALUE", 0, 
    "The initial x value"
  },
  { 
    "xf", 312, "VALUE", 0, 
    "The final x value"
  },
  { 
    "yi", 313, "VALUE", 0, 
    "The initial y value"
  },
  { 
    "yf", 314, "VALUE", 0, 
    "The final y value"
  },
  { 
    "static-node-exec", 315, 0, 0, 
    "Sets a static execution mode"
  },
  { 
    "dynamic-node-exec", 316, 0, 0, 
    "Sets a dynamic execution mode"
  },
  { 
    "work-stealing-node-exec", 318, 0, 0, 
    "Sets a work-stealing execution mode"
  },
  { 
    "no-histogram", 319, 0, 0, 
    "Disables the output of the color histogram"
  },
  { 
    "histogram", 320, "FILENAME", 0, 
    "Sets the output file of the histogram"
  },
  { 
    "no-print-histogram", 321, 0, 0, 
    "Does not print the histogram"
  },
  { 
    0 
  }
};


struct argp argp = 
{ 
  options, parse_opt, "", 
  "Computes and outputs the mandelbrot set.\v"
    "Be carefull! A 8192x8192 pixel image occupies more than 512 MB!"
};

struct execution_info ei = 
{
  .print_image          = false,
  .image_fn             = DEFAULT_OUTPUT,
  .join_output          = true,
  .omp_dynamic          = true,
  .eval                 = false,
  .runs                 = 10,
  .show_total_time      = false,
  .show_statistics      = true,
  .n_threads            = 1,
  .save_statistics      = false,
  .statistics_fn        = DEFAULT_STAT_OUTPUT,
  .use_omp              = true,
  .execution_mode       = STATIC_MODE,
  .histogram            = true,
  .histogram_fn         = DEFAULT_HISTOGRAM,
  .histogram_print      = true,
  .data = {
    .width                = DEFAULT_WIDTH,
    .len                  = DEFAULT_HEIGHT,
    .xi                   = -2.0,
    .xf                   =  2.0,
    .yi                   = -2.0,
    .yf                   =  2.0,
  },
  .s_i = 
    {
      .rt_avg           = true,
      .ut_avg           = true,
      .rt_v             = true,
      .ut_v             = true,
      .num_cpus         = true,
      .cpu_occupation   = true,
      .speed_up         = true,
      .discard          = 0.1f
    }
};

/* shows the execution info data */
void
show_exec_info_slave(struct execution_info_slave* eis)
{
  __yal_log("execution info slave:\n");
  __yal_log("\tjoin output:\t"NUM"\n"         , eis->join_output);
  __yal_log("\tprint image:\t"NUM"\n"         , eis->print_image);
  __yal_log("\timage name:\t%s\n"             , eis->image_fn);
  __yal_log("\texecution type:\t"NUM"\n"      , eis->execution_type);
  __yal_log("\tnumber of runs:\t"NUM"\n"      , eis->runs);
  __yal_log("\tnumber of threads:\t"NUM"\n"   , eis->n_threads);
  __yal_log("\tdynamic_parallelism:\t"NUM"\n" , eis->dynamic_parallelism);
  __yal_log("\texecution_mode:\t"NUM"\n"      , eis->execution_mode);
  show_mb_grid(&(eis->data));
}

void
create_mpi_execution_info_slave_type()
{
  struct execution_info_slave dummy;
  /* Number of elements in each position of the derivated datatype */ 
  number len[NUM_FIELDS_EXEC_INFO_SLAVE] =
  {
    1,
    1,
    MAX_FILENAME_SIZE,
    1,
    1,
    1,
    1,
    1,
    1,
    1
  }; 

  /* Getting the memory addresses to displacement */
  mpi_aint disp[NUM_FIELDS_EXEC_INFO_SLAVE]; 

  mpi_datatype type[NUM_FIELDS_EXEC_INFO_SLAVE] = 
  {
    mpi_unumber_t,
    mpi_unumber_t,
    mpi_char_t,
    mpi_unumber_t,
    mpi_unumber_t,
    mpi_unumber_t,
    mpi_unumber_t,
    mpi_unumber_t,
    mpi_unumber_t,
    mb_grid_datatype
  };

  mpi_get_address(&dummy.join_output, &disp[0]); 
  mpi_get_address(&dummy.print_image, &disp[1]); 
  mpi_get_address(&dummy.image_fn, &disp[2]); 
  mpi_get_address(&dummy.execution_type, &disp[3]);
  mpi_get_address(&dummy.runs, &disp[4]);
  mpi_get_address(&dummy.n_threads, &disp[5]);
  mpi_get_address(&dummy.execution_mode, &disp[6]);
  mpi_get_address(&dummy.histogram, &disp[7]);
  mpi_get_address(&dummy.dynamic_parallelism, &disp[8]);
  mpi_get_address(&dummy.data, &disp[9]);

  unumber i;
  
  for (i=1; i < NUM_FIELDS_EXEC_INFO_SLAVE; i++){
    disp[i] = disp[i] - disp[0];
  }
  disp[0] = 0;

  mpi_type_create_struct(NUM_FIELDS_EXEC_INFO_SLAVE, len, disp, type, &eis_datatype);
  mpi_type_commit(&eis_datatype);
}

void
destroy_mpi_execution_info_slave_type()
{
  mpi_type_free(&eis_datatype);
}

#endif /* __OPT_HELPER__ */