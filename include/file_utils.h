#include <stdio.h>
#include <stdlib.h>
#include "types.h"

#ifndef __FILE_UTILS__
#define __FILE_UTILS__

/* writes a buffer to a pgm file */
void
write_pgm(char*, byte*, unumber, unumber, unumber);

/* writes the histogram to a stream */
void
write_hist(FILE* f, unumber* hist);

#endif /* __FILE_UTILS__ */