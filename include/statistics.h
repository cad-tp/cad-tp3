#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "types.h"

#ifndef STATISTICS_H_
#define STATISTICS_H_

typedef struct
{
  //the tests real-times
  real * real_times;

  //the tests user-times
  real * user_times;

  //the current test_counter
  number test_counter;

  //total number of tests
  number num_tests;

  //number of workers
  number num_workers;

  //upper and lower percentage of results to be discarded.
  real filter;

} statistics_t;

statistics_t *
create_statistics(number nworkers, number ntests);

void
delete_statistics(statistics_t *);

void
set_statistics(boolean rt_avg_, boolean ut_avg_, boolean rt_v_, 
  boolean ut_v_, boolean num_cpus_, boolean cpu_occupation_, boolean speed_up_);

void
show_statistics(statistics_t *, statistics_t *);

void
save_statistics(statistics_t *, statistics_t *, char* filename);

void
set_filter(statistics_t *, real filter);

real
get_real_time_average(statistics_t *);

real
get_real_time_variance(statistics_t *);

real
get_user_time_average(statistics_t *);

real
get_user_time_variance(statistics_t *);

void
add_time(statistics_t*, real, real);

real
get_cpu_occupation(statistics_t*);

#endif
