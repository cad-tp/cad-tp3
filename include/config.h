#include <sys/sysinfo.h>

#ifndef __CONFIG__
#define __CONFIG__

#define MAIL_ADDRESS 		"guilhermemtr@gmail.com or to fv.pinheiro@campus.fct.unl.pt"
#define VERSION				"version 1.0"


#define DEFAULT_WIDTH 		(1 << 13)
#define DEFAULT_HEIGHT 		(1 << 13)
#define DEFAULT_OUTPUT		"a.pgm"
#define DEFAULT_STAT_OUTPUT	"a.csv"
#define DEFAULT_HISTOGRAM 	"a.hist"

#define num_cpus() get_nprocs_conf()

#define STATIC_MODE			0x00
#define DYNAMIC_MODE		0x01
#define WS_MODE				0x02

#define DEFAULT_WAIT_TIME	1000

#endif /* __CONFIG__ */